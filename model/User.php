<?php
require_once "Model.php";
require_once "UserCategory.php";
class User extends Model {
	
	public function checkAvailabilityAndGetUser($data)
	{
		$userName = mysqli_real_escape_string($this->connection, $data["username"]);
		$password = mysqli_real_escape_string($this->connection, $data["password"]);
		$sql = "SELECT * FROM users WHERE username='$userName' AND password='".md5($password)."'";
		$r_query = mysqli_query($this->connection, $sql);
		$userHandler = mysqli_fetch_assoc($r_query);
		return $userHandler;
	}
	
	public function createAndGetUser($data)
	{
		$userName = mysqli_real_escape_string($this->connection, $data["username"]);
		$password = mysqli_real_escape_string($this->connection, $data["password"]);
		$nickName = mysqli_real_escape_string($this->connection, $data["nickName"]);
		$categoryID = 0;
		$categoryHandler = new UserCategory();
		$category = $categoryHandler->getIDFor("learner");
		if($category)
		{
			$categoryID = $category["category_id"];
		}
		
		$sql = "INSERT INTO users SET username='$userName' , password='".md5($password)."',nickname='$nickName',category_id=$categoryID";
		$r_query = mysqli_query($this->connection, $sql);
		$userID = mysqli_insert_id($this->connection);
		return $userID;
	}
	
	public function getLastAttemptCount($userID)
	{
		$sql = "SELECT MAX(attempt_number) AS lastAttemptNumber FROM user_dice_roll_history WHERE user_id=$userID";
		$r_query = mysqli_query($this->connection, $sql);
		$attemptHandler = mysqli_fetch_assoc($r_query);
		return $attemptHandler;
	}
}