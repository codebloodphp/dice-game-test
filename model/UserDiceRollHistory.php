<?php
require_once "Model.php";
class UserDiceRollHistory extends Model{
	
	public function updateScore($params)
	{
		$userID = $_SESSION["User"]["id"];
		$point_score = $params["point_score"];
		$attempt_number = $_SESSION["User"]["attemptNumber"];

		$sql = "INSERT INTO user_dice_roll_history SET user_id=".$userID." , attempt_number=".$attempt_number.",point_score=".$point_score.",timetaken='".time()."'";
		$r_query = mysqli_query($this->connection, $sql);
		$scoreID = mysqli_insert_id($this->connection);
		return $scoreID;
	}
	
	public function getTotalScore(){
		$userID = $_SESSION["User"]["id"];
		$attemptNumber = $_SESSION["User"]["attemptNumber"];
		$sql = "SELECT SUM(point_score) AS totalScore FROM user_dice_roll_history WHERE user_id=$userID AND attempt_number=$attemptNumber";
		$r_query = mysqli_query($this->connection, $sql);
		$scoreHandler = mysqli_fetch_assoc($r_query);
		return $scoreHandler["totalScore"];
	}
	
	public function getLeaderboardData(){
		$sql = "SELECT 
					u.nickname,
					SUM(udrh.point_score) AS 'TotalScore',
					udrh.attempt_number,
					(MAX(udrh.timetaken) - MIN(udrh.timetaken)) AS 'TimeTakenInSec'
				FROM
					user_dice_roll_history udrh
						INNER JOIN
					users u ON u.user_id = udrh.user_id
				GROUP BY udrh.user_id , udrh.attempt_number ORDER BY TotalScore DESC , TimeTakenInSec ASC ";
		$r_query = mysqli_query($this->connection, $sql);
		
		$data = [];
		while($row = mysqli_fetch_assoc($r_query))
		{
			$data[] = $row;
		}
		
		return $data;
	}
}