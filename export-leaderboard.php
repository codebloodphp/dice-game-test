<?php
session_start();
if(!$_SESSION) {
	header("Location:index.php");
}

if(!$_SESSION["IsAdmin"])
{
	header("Location:index.php");
}

require_once "model/UserDiceRollHistory.php";
$leaderboardHandler = new UserDiceRollHistory();
$dataList = $leaderboardHandler->getLeaderboardData();

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="leader-board.csv";');

$fp = fopen('php://output', 'w');
$header = ["Nick Name","Total Score","Attempt Number","TimeTaken In Sec"];
fputcsv($fp, $header);

foreach ($dataList as $fields) {
    fputcsv($fp, $fields);
}
fclose($fp);
exit;