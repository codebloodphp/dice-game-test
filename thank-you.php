<?php
session_start();
if(!$_SESSION) {
	header("Location:index.php");
}

if($_SESSION["counter"]>0)
{
	header("Location:index.php");
}

require_once "model/UserDiceRollHistory.php";
$scoreHandler = new UserDiceRollHistory();
$totalAttemptScore = $scoreHandler->getTotalScore();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Indusgeeks - Dice!</title>
  </head>
	<body>
		<div class="container m-5 text-center">
			<div class="alert alert-success" role="alert">
			  THANK YOU , Your total attempt scores is <?php echo $totalAttemptScore;?> 
			</div>
			<div id="button-trigger" class="mt-5">
					<button id="trigger" class="btn btn-primary" onclick="javascript:location='play-again.php'">PLAY AGAIN</button>
					
					<button id="trigger-logout" class="btn btn-danger" onclick="javascript:location='logout.php';">LOGOUT</button>
			</div>
		</div>
	</body>
</html>