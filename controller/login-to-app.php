<?php
session_start();
require_once "../model/User.php";
if(!empty($_POST))
{
	//print_r($_POST); exit;
	//-- Check if admin login attempt.
	$userName = $_POST["username"];
	$password = $_POST["password"];
	//var_dump(($userName=="admin" && $password=="admin")); exit;
	if($userName=="admin" && $password=="admin")
	{
		//-- Its an admin , redirect to leaderboard dashboard..
		$_SESSION["IsAdmin"] = TRUE;
		header("Location:../leader-board-view.php");
		exit;
	}
	
	$attemptCount = 1;
	//-- Check if the learner is already register..
	$userModel = new User();
	$userHandler = $userModel->checkAvailabilityAndGetUser($_POST);
	if(!$userHandler)
	{
		//-- Create new learner.
		$userID = $userModel->createAndGetUser($_POST);
	}else{
			$userID = $userHandler["user_id"]; //-- Already registerted player.
			$attemptCountHandler = $userModel->getLastAttemptCount($userID);
			if($attemptCountHandler["lastAttemptNumber"])
				$attemptCount=$attemptCountHandler["lastAttemptNumber"]+1;
		}
	
	//-- Auto Login And Redirect To Game Play.
	$user["id"] = $userID;
	$user["username"] = $_POST["username"];
	$user["nickname"] = $_POST["nickName"];
	$user["attemptNumber"] = $attemptCount;
	$_SESSION["User"] = $user;
	$_SESSION["counter"]=0;
	unset($user);
	header("Location:../game-play.php");
}