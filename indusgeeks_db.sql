-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2021 at 04:52 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `indusgeeks_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(4) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `nickname`, `category_id`, `created_at`) VALUES
(1, 'sanjay.yadav', 'c6e8b6b84207eac794d5ab5e007149f0', 'Sanjay', 1, '2021-06-27 13:00:10'),
(2, 'dsfsdf', '73a90acaae2b1ccc0e969709665bc62f', 'sdfsdfsdf', 1, '2021-06-27 15:55:47'),
(3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1, '2021-06-27 15:59:18'),
(4, 'sonu', 'e555f863fb09593119fe2f3459e9783a', 'sonu', 1, '2021-06-27 16:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_category`
--

CREATE TABLE `user_category` (
  `category_id` int(4) NOT NULL,
  `Title` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_category`
--

INSERT INTO `user_category` (`category_id`, `Title`) VALUES
(1, 'learner');

-- --------------------------------------------------------

--
-- Table structure for table `user_dice_roll_history`
--

CREATE TABLE `user_dice_roll_history` (
  `id` int(4) NOT NULL,
  `user_id` int(4) NOT NULL,
  `attempt_number` int(4) NOT NULL,
  `point_score` int(4) NOT NULL,
  `timetaken` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_dice_roll_history`
--

INSERT INTO `user_dice_roll_history` (`id`, `user_id`, `attempt_number`, `point_score`, `timetaken`) VALUES
(1, 1, 1, 6, 1624806887),
(2, 1, 1, 1, 1624806960),
(3, 1, 1, 5, 1624807013),
(4, 1, 2, 2, 1624808589),
(5, 1, 2, 2, 1624808823),
(6, 1, 2, 6, 1624808835),
(7, 1, 3, 2, 1624808862),
(8, 1, 3, 1, 1624808872),
(9, 1, 3, 5, 1624808880),
(10, 2, 1, 1, 1624809512),
(11, 2, 1, 4, 1624809520),
(12, 2, 1, 1, 1624809527),
(13, 3, 1, 5, 1624809614),
(14, 3, 1, 4, 1624809620),
(15, 3, 1, 2, 1624809626),
(16, 3, 2, 3, 1624809703),
(17, 3, 2, 6, 1624809708),
(18, 3, 2, 5, 1624809714),
(19, 3, 3, 4, 1624809808),
(20, 3, 3, 2, 1624809816),
(21, 3, 3, 6, 1624809823),
(22, 3, 4, 2, 1624810078),
(23, 3, 4, 1, 1624810083),
(24, 3, 4, 3, 1624810089),
(25, 4, 1, 2, 1624812615),
(26, 4, 1, 4, 1624812624),
(27, 4, 1, 4, 1624812632);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_category`
--
ALTER TABLE `user_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `user_dice_roll_history`
--
ALTER TABLE `user_dice_roll_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_category`
--
ALTER TABLE `user_category`
  MODIFY `category_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_dice_roll_history`
--
ALTER TABLE `user_dice_roll_history`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
