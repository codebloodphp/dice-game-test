<?php
session_start();

if(!$_SESSION)
{
	$resp["status_code"]=400;
	$resp["message"] = "Session invalid";
	echo json_encode($resp); exit;
}

require_once "model/UserDiceRollHistory.php";
const STEP = 3;
if(!empty($_POST))
{
	//print_r($_POST); exit;
	if(!isset($_POST["score"]) || !is_numeric($_POST["score"]))
	{
		$resp["status_code"]=400;
		$resp["message"] = "Invalid paramter";
	}else{
			$params['point_score'] = $_POST["score"];
			$diceHandler = new UserDiceRollHistory();
			$newScoreID = $diceHandler->updateScore($params);
			if($newScoreID)
			{
				$resp["status_code"]=200;
				$resp["message"] = "Score updated";
				$resp["currentScore"] = $_POST["score"];
				$resp["totalScore"] = $diceHandler->getTotalScore();
				//--
				$_SESSION["counter"]+=1;
				if($_SESSION["counter"]>=STEP)
				{
					$resp["all_attempt_done"] = 1;
					$_SESSION["counter"] = 0; //-- Initialize for next attempt.
				}else{
					$resp["all_attempt_done"] = 0;
				}
			}else{
				$resp["status_code"]=400;
				$resp["message"] = "Fail to update";
			}
		}
}else{
	$resp["status_code"]=400;
	$resp["message"] = "Invalid paramter";
}
echo json_encode($resp); exit;