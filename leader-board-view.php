<?php
session_start();
if(!$_SESSION) {
	header("Location:index.php");
}

if(!$_SESSION["IsAdmin"])
{
	header("Location:index.php");
}

require_once "model/UserDiceRollHistory.php";
$leaderboardHandler = new UserDiceRollHistory();
$dataList = $leaderboardHandler->getLeaderboardData();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Indusgeeks - Dice!</title>
  </head>
	<body>
		<div class="container m-5 text-center">
			<h3>Leaderboard - Admin View</h3>
			<table class="table">
			  <thead>
				<tr>
				  <th scope="col">#</th>
				  <th scope="col">Nick Name</th>
				  <th scope="col">Total Score</th>
				  <th scope="col">Attempt Number</th>
				  <th scope="col">TimeTaken In Sec</th>
				</tr>
			  </thead>
			  <tbody>
				<?php foreach($dataList as $key=>$val) { ?>
				<tr>
				  <th scope="row"><?php echo $key+1;?></th>
				  <td><?php echo $val["nickname"];?></td>
				  <td><?php echo $val["TotalScore"];?></td>
				  <td><?php echo $val["attempt_number"];?></td>
				  <td><?php echo $val["TimeTakenInSec"];?></td>
				</tr>
				<?php } ?>
			  </tbody>
			</table>
			<div id="button-trigger" class="mt-5">
					<button id="trigger" class="btn btn-primary" onclick="javascript:location='export-leaderboard.php'">EXPORT</button>
					
					<button id="trigger-logout" class="btn btn-danger" onclick="javascript:location='logout.php';">LOGOUT</button>
			</div>
		</div>
	</body>
</html>