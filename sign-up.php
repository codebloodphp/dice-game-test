<div class="container m-5 text-center">
<form method="POST" id="entry-form" action="controller/login-to-app.php" onsubmit="return loginCredntial();">
  <div class="form-group row">
    <label for="username" class="col-sm-2 col-form-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="username" name="username" required placeholder="Username">
    </div>
  </div>
  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="password" name="password" required placeholder="Password">
    </div>
  </div>
  <div class="form-group row">
    <label for="nickName" class="col-sm-2 col-form-label">NickName</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nickName" name="nickName" required placeholder="Nick name">
    </div>
  </div>
  <div class="form-group row">
	<div class="col-sm-10 ">
		<button type="submit" class="btn btn-light float-right ml-5">LOGIN</button>
	</div>
  </div>
</form>
</div>
<script type="text/javascript">
function loginCredntial(){
	return true;
}
</script>