<?php
session_start();
if(!$_SESSION)
{
	header("index.php");
}

const STEP = 3;
$initialDice = rand(1,6);
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Indusgeeks - Dice!</title>
  </head>
	<body>
		<div class="container m-5 text-center">
			<h3  class="">Game &nbsp;&nbsp;<span id="current_step">0</span>&nbsp;/&nbsp;<span id="total_step"><?php echo STEP;?></span></h3>
			<div class="mt-3">
				<div id="dice-container">
					<img id="diceImg" src="assets/<?php echo $initialDice?>.png" style="max-height:150px;">
					<input type="hidden" id="diceScore" value="0">
				</div>
				<div id="button-trigger" class="mt-5">
					<button id="trigger" class="btn btn-light" onclick="rollDice();">RANDOM</button>
				</div>
			</div>
		</div>
	</body>
	<script src="jquery-3.6.0.min.js"></script>
	<script type="text/javascript">
	var fiveSecondTrigger;
	var counter=0;
		function rollDice()
		{
			$("#current_step").html(++counter);
			var nextDice = Math.floor(Math.random() * 6) + 1;
			console.log("New Dice is "+nextDice);
			document.getElementById("diceImg").setAttribute("src","assets/"+nextDice+".png");
			$("#diceScore").val(nextDice);
			sentAfterFiveSec();
		}
		
		function sentAfterFiveSec()
		{
			$("#trigger").prop("disabled",true);
			fiveSecondTrigger = setTimeout(updateScore, 5000);
		}
		
		function updateScore()
		{
			clearTimeout(fiveSecondTrigger);
			var currentScore = $("#diceScore").val();
			console.log("Sending currentScore "+currentScore);
			$.ajax({
					url:"updateScore.php",
					data:{score:currentScore},
					method:'post',
				}).done(function(resp) {
					var response = jQuery.parseJSON(resp);
					console.log("Status Code "+response.status_code);
					if(response.status_code==200)
					{
						$("#trigger").prop("disabled",false);
					}
					
					if(response.all_attempt_done==1)
					{
						//-- Reidrect to Thanks page.
						window.location = "thank-you.php";
					}
				});
		}
	</script>
</html>